﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace SQLSingleton
{
    public class SQL
    {
        private static SQL _sql = null;
        private static SqlConnection _con;

        public static SQL Instance
        {
            get
            {
                if (_sql == null)
                    Init();
                return _sql;
            }
        }
        //public static SqlTransaction CurrentTransaction { get; set; }

        public static bool Opened => _con != null && _con.State == ConnectionState.Open;

        private SQL(string connectionString)
        {
            _con = new SqlConnection(connectionString);
        }
        public static bool Init(string connectionString = null)
        {
            if (string.IsNullOrEmpty(connectionString) || !TestConnection(connectionString))
            {
                SqlConnectionForm scf = new SqlConnectionForm();
                if (scf.ShowDialog() == DialogResult.OK)
                    _sql = new SQL(scf.ConnectionString);
                else
                {
                    _sql = null;
                    return false;
                }
            }
            else
                _sql = new SQL(connectionString);
            
            return Open();
        }

        public static bool TestConnection(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                return false;
            else
                using (SqlConnection tmpCon = new SqlConnection(connectionString))
                    try
                    {
                        tmpCon.Open();
                        tmpCon.Close();
                        return true;
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                        return false;
                    }
        }

        public static bool Open()
        {
            if (_con == null)
                return false;

            if (Opened)
                return true;

            try
            {
                _con.Open();
            }
            catch (Exception) { }

            return true;
        }
        public static bool Close()
        {
            if (_con == null || _con.State == ConnectionState.Closed)
                return true;

            try
            {
                _con.Close();
            }
            catch (Exception) { }

            return Opened;
        }
        /*
        public static SqlTransaction BeginTransaction(string transactionName = null)
        {
            if (CurrentTransaction != null)
                return CurrentTransaction;

            if (Opened)
                return CurrentTransaction = string.IsNullOrWhiteSpace(transactionName) ? 
                    _con.BeginTransaction() : 
                    _con.BeginTransaction(transactionName);
            else
                return null;
        }
        public static bool CommitTransaction()
        {
            if (CurrentTransaction == null)
                return false;

            try
            {
                CurrentTransaction.Commit();
                return true;
            }
            catch
            {
                CurrentTransaction.Rollback();
                return false;
            }
        }
        public static bool SaveTransaction(string savePointName)
        {
            if (CurrentTransaction == null || string.IsNullOrWhiteSpace(savePointName))
                return false;

            try
            {
                CurrentTransaction.Save(savePointName);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool RollBackTransaction(string transactionOrSavePointName)
        {
            if (CurrentTransaction == null || string.IsNullOrWhiteSpace(transactionOrSavePointName))
                return false;

            try
            {
                CurrentTransaction.Rollback(transactionOrSavePointName);
                return true;
            }
            catch
            {
                return false;
            }
        }
        */
        public int ExecuteCommand(string command)
        {
            return ExecuteCommand(new SqlCommand(command));
        }
        public int ExecuteCommand(SqlCommand command)
        {
            int affected = -1;

            command.Connection = _con;

            try
            {
                Open();
                //if (CurrentTransaction != null)
                //    command.Transaction = CurrentTransaction;
                affected = command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }

            return affected;
        }

        public bool Exists(string tableName, NameValuePair where)
        {
            return Exists(tableName, Global.Conjunction.And, new NameValuePair[] { where });
        }
        public bool Exists(string tableName, params NameValuePair[] where)
        {
            return Exists(tableName, Global.Conjunction.And, where);
        }
        public bool Exists(string tableName, Global.Conjunction conjunction, params NameValuePair[] where)
        {
            var whereList = where.ToList();
            SqlCommand command = new SqlCommand("SELECT COUNT(1) FROM[" + tableName + "] WHERE " 
                + string.Join(Global.ConjunctionOperatorDict[conjunction], whereList));

            whereList.ForEach(x => { x.NameValuePairType = NameValuePair.NameValuePairTypes.Where; command.Parameters.Add(x.SqlParameter); });
            
            return 1 == (int)SelectScalar(command);
        }

        public int Insert(string tableName, params NameValuePair[] valuePairs)
        {
            if (valuePairs == null || valuePairs.Count() == 0)
                return -1;

            SqlCommand command = new SqlCommand("INSERT INTO " + tableName + " ("+
                string.Join(",", valuePairs.Select(x => x.Left)) + ") VALUES (" +
                string.Join(",", valuePairs.Select(x => x.RightString)) + ")"
                );
            
            command.Parameters.AddRange(valuePairs.Select(x => x.SqlParameter).ToArray());

            return ExecuteCommand(command);
        }
        
        public int Delete(string tableName, params NameValuePair[] where)
        {
            return Delete(tableName, Global.Conjunction.And, where);
        }
        public int Delete(string tableName, Global.Conjunction conjunction, params NameValuePair[] where)
        {
            SqlCommand command = new SqlCommand("DELETE FROM " + tableName);

            if (where == null || where.Count() == 0)
                return ExecuteCommand(command);

            var whereList = where.ToList();
            whereList.ForEach(x => { x.NameValuePairType = NameValuePair.NameValuePairTypes.Where; command.Parameters.Add(x.SqlParameter); });

            command.CommandText += " WHERE " + string.Join(Global.ConjunctionOperatorDict[conjunction], whereList);

            return ExecuteCommand(command);
        }

        public int Update(string tableName, params NameValuePair[] set)
        {
            if (set == null || set.Count() == 0)
                return -1;

            return Update(tableName, Global.Conjunction.And, null, set);
        }
        public int Update(string tableName, List<NameValuePair> set, NameValuePair where)
        {
            if (set == null || set.Count() == 0)
                return -1;

            return Update(tableName, Global.Conjunction.And, new List<NameValuePair> { where }, set.ToArray());
        }
        public int Update(string tableName, Global.Conjunction conjunction, List<NameValuePair> where, params NameValuePair[] set)
        {
            if (set == null || set.Count() == 0)
                return -1;

            SqlCommand command = new SqlCommand("UPDATE " + tableName);

            var setList = set.ToList();
            setList.ForEach(x => {
                x.NameValuePairType = NameValuePair.NameValuePairTypes.Set;
                x.ComparisonOperator = Global.ComparisonOperator.Equals;
            });

            command.CommandText += " SET " + string.Join(", ", setList);
            command.Parameters.AddRange(setList.Select(x => x.SqlParameter).ToArray());

            if (where != null && where.Count > 0)
            {
                var whereList = where.ToList();
                whereList.ForEach(x => x.NameValuePairType = NameValuePair.NameValuePairTypes.Where);

                command.CommandText += " WHERE " + string.Join(Global.ConjunctionOperatorDict[conjunction], whereList);
                command.Parameters.AddRange(whereList.Select(x => x.SqlParameter).ToArray());
            }

            return ExecuteCommand(command);
        }

        public object SelectScalar(string tableName, string value, params NameValuePair[] where)
        {
            return SelectScalar(tableName, value, Global.Conjunction.And, where);
        }
        public object SelectScalar(string tableName, string value, Global.Conjunction conjunction, params NameValuePair[] where)
        {
            SqlCommand command = new SqlCommand("SELECT " + value + " FROM " + tableName);

            if (where != null && where.Count() > 0)
            {
                var whereList = where.ToList();
                whereList.ForEach(x => x.NameValuePairType = NameValuePair.NameValuePairTypes.Where);

                command.CommandText += " WHERE " + string.Join(Global.ConjunctionOperatorDict[conjunction], whereList);
                command.Parameters.AddRange(whereList.Select(x => x.SqlParameter).ToArray());
            }

            return SelectScalar(command);
        }
        public object SelectScalar(string command)
        {
            return SelectScalar(new SqlCommand(command));
        }
        public object SelectScalar(SqlCommand command)
        {
            command.Connection = _con;

            object result = null;

            try
            {
                Open();
                result = command.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }

            return result;
        }

        public DataSet Select(string tableName, params string[] values)
        {
            return Select(tableName, values.ToList(), Global.Conjunction.And, null);
        }
        public DataSet Select(string tableName, NameValuePair where, params string[] values)
        {
            return Select(tableName, values.ToList(), Global.Conjunction.And, new NameValuePair[] { where });
        }
        public DataSet Select(string tableName, List<string> values, params NameValuePair[] where)
        {
            return Select(tableName, values, Global.Conjunction.And, where);
        }
        public DataSet Select(string tableName, List<string> values, Global.Conjunction conjunction, params NameValuePair[] where)
        {
            if (values == null || values.Count() == 0)
                return null;

            SqlCommand command = new SqlCommand("SELECT ");

            command.CommandText += string.Join(", ", values);

            command.CommandText += " FROM " + tableName;

            if (where != null && where.Count() > 0)
            {
                var whereList = where.ToList();
                whereList.ForEach(x => x.NameValuePairType = NameValuePair.NameValuePairTypes.Where);

                command.CommandText += " WHERE " + string.Join(Global.ConjunctionOperatorDict[conjunction], whereList);
                command.Parameters.AddRange(whereList.Select(x => x.SqlParameter).ToArray());
            }

            return Select(command);
        }
        public DataSet Select(string command)
        {
            return Select(new SqlCommand(command));
        }
        public DataSet Select(SqlCommand command)
        {
            command.Connection = _con;

            DataSet result = new DataSet();

            SqlDataAdapter sda = new SqlDataAdapter(command);

            try
            {
                Open();
                sda.Fill(result);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }

            return result;
        }
    }
}
