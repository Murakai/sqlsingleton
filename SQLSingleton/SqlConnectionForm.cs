﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLSingleton
{
    public partial class SqlConnectionForm : Form
    {
        private SqlConnectionControl connectionControl1;

        public string HostName => connectionControl1.HostName;
        public string DatabaseName => connectionControl1.DatabaseName;
        public bool IntegratedSecurity => connectionControl1.IntegratedSecurity;
        public string UserName => connectionControl1.UserName;
        public string Password => connectionControl1.Password;

        public string ConnectionString => connectionControl1.ConnectionString;

        public bool Result { get; private set; }

        public SqlConnectionForm(bool allowIntegratedSecurity = true)
        {
            InitializeComponent();

            connectionControl1 = new SqlConnectionControl(allowIntegratedSecurity);
            connectionControl1.OnCalledClose += ConnectionControl1OnOnCalledClose;
            elementHost1.Child = connectionControl1;
        }

        public SqlConnectionForm(string hostName, string dataBaseName, string userName = "sa", bool allowIntegratedSecurity = true)
        {
            InitializeComponent();

            connectionControl1 = new SqlConnectionControl(hostName, dataBaseName, userName, allowIntegratedSecurity);
            connectionControl1.OnCalledClose += ConnectionControl1OnOnCalledClose;
            elementHost1.Child = connectionControl1;
        }

        private void ConnectionControl1OnOnCalledClose(object sender, ConnectionControlCalledCloseEventArgs args)
        {
            DialogResult = (Result = args.Result) ? DialogResult.OK : DialogResult.Abort;
            Close();
        }
        /*
        private RegistryData TestConnection (List<RegistryData> registryDataList, bool allowIntegratedSecurity = true)
        {
            foreach (var regData in registryDataList)
                if (TestConnection(regData, allowIntegratedSecurity))
                    return regData;

            return null;
        }
        private bool TestConnection(RegistryData registryData, bool allowIntegratedSecurity = true)
        {
            var scb = new SqlConnectionStringBuilder();
            scb.DataSource = registryData.Hostname;
            scb.InitialCatalog = registryData.Database;

            if (string.IsNullOrWhiteSpace(registryData.Password))
                scb.IntegratedSecurity = true;
            else
            {
                if (!allowIntegratedSecurity)
                    return false;

                scb.UserID = registryData.Username;
                scb.Password = registryData.Password;
            }

            return TestConnection(scb.ConnectionString);
        }
        */
        public static bool TestConnection(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                return false;
            else
                using (SqlConnection tmpCon = new SqlConnection(connectionString))
                    try
                    {
                        tmpCon.Open();
                        tmpCon.Close();
                        return true;
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                        return false;
                    }
        }
    }
    /*
    public class RegistryData
    {
        public string Hostname { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        //public string Password { get; set; }

        public RegistryData(string hostname = "", string database = "", string username = "")//, string password = "")
        {

        }

        public static RegistryData GetSpecificDatabaseConnection(int id = 0)
        {
            var dbKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry32);
            dbKey = dbKey.OpenSubKey("SOFTWARE\\AMTANGEE\\CRM\\" + (id > 0 ? "DBs\\" + id : "DB"), false);

            if (dbKey == null)
                return null;

            var newCon = new RegistryData();
            newCon.Hostname = (string)dbKey.GetValue("Hostname", "");
            newCon.Database = (string)dbKey.GetValue("Database", "");
            newCon.Username = (string)dbKey.GetValue("Username", "");
            /*
            var pw = (string)dbKey.GetValue("Password", "");
            if (pw == "237209230019193238145106089241078")
                newCon.Password = "Amtangee99#";
            */
            /*
            return newCon;
        }

        public static List<RegistryData> GetDBConnectionsFromRegistry()
        {
            var connections = new List<RegistryData>();

            connections.Add(GetSpecificDatabaseConnection(0));

            var regKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry32);
            regKey = regKey.OpenSubKey("SOFTWARE\\AMTANGEE\\CRM\\DBs", false);

            if (regKey == null)
                return connections;

            using (var dbsKey = regKey.OpenSubKey("DBs"))
                if (dbsKey != null)
                    foreach (var item in dbsKey.GetSubKeyNames())
                        connections.Add(GetSpecificDatabaseConnection(int.Parse(item)));

            return connections;
        }
    }
    */
}
