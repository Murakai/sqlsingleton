﻿using System;

namespace SQLSingleton.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class Field : Attribute
    {
        public string FieldName { get; private set; }
        public bool LoadLater { get; private set; }
        public bool IsEncrypted { get; private set; }

        public Field(string fieldName, bool loadLater = false, bool isEncrypted = false)
        {
            FieldName = fieldName;
            LoadLater = loadLater;
            IsEncrypted = isEncrypted;
        }
    }
}
