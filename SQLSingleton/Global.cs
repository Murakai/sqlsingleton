﻿using System.Collections.Generic;

namespace SQLSingleton
{
    public class Global
    {
        public enum ComparisonOperator
        {
            Equals,
            NotEquals,
            GreaterThan,
            GreaterThanOrEqual,
            LessThan,
            LessThanOrEqual,
            In,
            NotIn,
            Is
        }

        public static Dictionary<ComparisonOperator, string> ComparisonOperatorDict = new Dictionary<ComparisonOperator, string>
        {
            { ComparisonOperator.Equals, "=" },
            { ComparisonOperator.NotEquals, "<>" },
            { ComparisonOperator.In, "in" },
            { ComparisonOperator.NotIn, "not in" },
            { ComparisonOperator.GreaterThan, ">" },
            { ComparisonOperator.GreaterThanOrEqual, ">=" },
            { ComparisonOperator.LessThan, "<" },
            { ComparisonOperator.LessThanOrEqual, "<=" },
            { ComparisonOperator.Is, "is" },
        };

        public enum Conjunction
        {
            And,
            Or
        }

        public static Dictionary<Conjunction, string> ConjunctionOperatorDict = new Dictionary<Conjunction, string>
        {
            { Conjunction.And, " and " },
            { Conjunction.Or, " or " },
        };
    }
}
