﻿namespace SQLSingleton
{
    public class NameValuePair
    {
        public enum NameValuePairTypes
        {
            Where,
            Set
        }

        public string Left { get; set; }
        public object Right { get; set; }
        public string RightString => (NameValuePairType == NameValuePairTypes.Where ? "@w" : "@s") + Left;
        public NameValuePairTypes NameValuePairType;
        public Global.ComparisonOperator ComparisonOperator;

        public System.Data.SqlClient.SqlParameter SqlParameter =>
            new System.Data.SqlClient.SqlParameter(RightString, Right);

        public NameValuePair(string left, object right, Global.ComparisonOperator comparisonOperator = Global.ComparisonOperator.Equals, NameValuePairTypes nameValuePairType = NameValuePairTypes.Where)
        {
            Left = left;
            Right = right;
            NameValuePairType = nameValuePairType;
            ComparisonOperator = comparisonOperator;
        }

        public override string ToString()
        {
            return Left + Global.ComparisonOperatorDict[ComparisonOperator] + RightString;
        }
    }
}
