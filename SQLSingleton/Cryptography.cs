﻿#region changes to this files wont be tracked (to protect the keys)!!! "git update-index --no-assume-unchanged SnippetCollection/Cryptography.cs" to revert! ("git update-index --assume-unchanged SnippetCollection/Cryptography.cs" to ignore again)
using System;
using System.Security.Cryptography;
using System.Text;

namespace SQLSingleton
{
    public static class Cryptography
    {
        //used for creation: http://www.unit-conversion.info/texttools/random-string-generator/
        public static string Key { private get; set; } =
        #region 32 chars -> 256 bit
            "KrvwMv2Q69ojXTdngKXUcErpZ6O3u3Qf"; //32 chars -> 256 bit
        #endregion
        public static string IV { private get; set; } =
        #region 16 chars -> 128 bit
            "376bZFBTQ1rsZWBE"; //16 chars -> 128 bit
        #endregion

        internal static string Decrypt(this string encryptedText)
        {
            if (string.IsNullOrWhiteSpace(encryptedText))
                return null;

            var encryptedbytes = Convert.FromBase64String(encryptedText);

            var acsp = new AesCryptoServiceProvider
            {
                BlockSize = 128,
                KeySize = 256,
                Key = ASCIIEncoding.ASCII.GetBytes(Key),
                IV = ASCIIEncoding.ASCII.GetBytes(IV),
                Padding = PaddingMode.PKCS7,
                Mode = CipherMode.CBC,
            };

            var ict = acsp.CreateDecryptor(acsp.Key, acsp.IV);

            var encryptedBytes = ict.TransformFinalBlock(encryptedbytes, 0, encryptedbytes.Length);

            ict.Dispose();

            return ASCIIEncoding.ASCII.GetString(encryptedBytes);
        }

        public static string Encrypt(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            var textBytes = ASCIIEncoding.ASCII.GetBytes(text);

            var acsp = new AesCryptoServiceProvider {
                BlockSize = 128,
                KeySize = 256,
                Key = ASCIIEncoding.ASCII.GetBytes(Key),
                IV = ASCIIEncoding.ASCII.GetBytes(IV),
                Padding = PaddingMode.PKCS7,
                Mode = CipherMode.CBC,
            };

            var ict = acsp.CreateEncryptor(acsp.Key, acsp.IV);

            var encryptedBytes = ict.TransformFinalBlock(textBytes, 0, textBytes.Length);

            ict.Dispose();

            return Convert.ToBase64String(encryptedBytes);
        }
    }
}
#endregion